import React from 'react'
import styled, { css } from 'styled-components'
import { Container, Row, Col } from 'react-awesome-styled-grid'
import siteConfig from '../../../data/siteConfig'
import { withPrefix } from "gatsby"
import loadable from '@loadable/component'

const Image = styled.img`
  max-height: 295px;
  max-width: 220px;
  object-fit: cover;
  object-position: center center;
  border-radius: 10px;
  box-shadow: 24px 47px 79px -21px rgba(0,0,0,0.51);
`

const Text = styled.p`
  font-weight: bold;
  text-align: center;
  font-size: 13px;
`

const H2 = styled.h2 `
  font-size: 2.25rem;
  text-align: center;
`;

const JobCard = styled.a`
  text-decoration: none;
  color: inherit;

  ${({ href }) => href && css`
    &:hover ${Image}{
      transition: transform .5s;
      transform: translateY(-5px);
    }
  `}
`

const Repositories = ({ className, location }) => {
  const title = "Portifolio"
  const { keywords, portifolio } = siteConfig
  return (
      <>
      <H2>Портфолио</H2>
        <Container className="page-content" fluid>
          <Row>
            {portifolio.map(job => (
              <Col
                key={job.description}
                align="center"
              >
                <JobCard
                  as={job.url ? "a" : "div"}
                  href={job.url}
                  target="_blank"
                >
                  <Image src={withPrefix(job.image)} className="someImg" />
                  <Text>{job.description}</Text>
                </JobCard>
              </Col>
            ))}
          </Row>
        </Container>
        </>
  )
}

export default styled(Repositories)`
  .page-content {
    max-width: 100%;
    margin-bottom: 40px;
  }

  .h1-portfolio {
    margin-bottom: 46px;
  }

`

