import React from 'react';
import styled, { css } from 'styled-components'

const TextAbout = styled.p`
  text-align: justify
`

export default ({ text = '' }) => {
  return (
    <div>
      <TextAbout  dangerouslySetInnerHTML={{ __html: text }}></TextAbout>
    </div>
  )
}


