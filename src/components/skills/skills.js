import React, { useEffect } from 'react'
import styled from 'styled-components'
import SkillBar from './skill-bar'

const H2 = styled.h2 `
  font-size: 2.25rem;
  text-align: center;
`;

export default styled(({ className, title = 'Skills', skills = [] }) => (
  <div className={className}>
    <H2>{title}</H2>
    {skills.map(skill => (
      <SkillBar key={skill.name} name={skill.name} level={skill.level} />
    ))}
  </div>
))`
  width: 100%;
`
