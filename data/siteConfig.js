module.exports = {
  siteTitle: '',
  siteDescription: `Create your online curriculum in just a few minutes with this starter`,
  keyWords: ['gatsbyjs', 'react', 'curriculum'],
  authorName: 'Ренат Хакимов',
  twitterUsername: '_franciscodf',
  githubUsername: 'santosfrancisco',
  authorAvatar: '/images/avatar.jpeg',
  authorDescription: `&nbsp;Мобильный разработчик, увлеченный своим делом. <br />
    Начинал в 2018 г. как весрстальщик на стажировке в компании. <br />
  &nbsp;&nbsp;В начале 2019 г. устролся в команию на позицию junior frontnend разработчик(React.js), занимался небольшим SPA проектами.
  &nbsp;&nbsp;В марте 2020 г. ушел во фриланс. Также делал небольшие SPA проекты на <strong>React.js</strong>. В совокупности реализовал чуть больше 10 проектов. <br />
  &nbsp;&nbsp;В настоящее время активно занимаюсь мобильной разработкой. <br />
  &nbsp;&nbsp;Люблю спорт, познавать новое, сейчас акцентирован больше на computer science и мобильной разработке(<strong>React Native и нативные платформы</strong>).`,
  skills: [
    {
      name: 'React.js',
      level: '80'
    },
    {
      name: 'React Native',
      level: '60'
    },
    {
      name: 'Javascript',
      level: '80'
    },
    {
      name: 'NodeJs',
      level: '40'
    },
    {
      name: 'HTML',
      level: '90'
    },
    {
      name: 'Git',
      level: 90
    },
    /* more skills here */
  ],
  jobs: [
    /* more jobs here */
    {
      company: '',
      begin: {
        month: 'сен',
        year: '2018'
      },
      duration: '3 мес',
      occupation: "Верстальщик в продуктовой компании",
      description: "Делал верстку компонентов на проекте (React.js)"
  
    },  {
      company: "",
      begin: {
        month: 'янв',
        year: '2019'
      },
      duration: '1 год и 2 мес',
      occupation: "Frontend разработчик в аутсорсинговой компании",
      description: "Отвечал за создание небольших SPA проектов на React.js. Реализовал чуть болльше 10 проектов. "
  
    }, {
      company: "",
      begin: {
        month: 'мар',
        year: '2020'
      },
      duration: 'по настоящее время',
      occupation: "Frontend разработчик, мобильный разработчик",
      description: "Делал небольшие проекты на JS, React.js, React Native, WordPress"
    }
  ],
  portifolio: [
    {
      image: "/images/first.jpeg",
      description: "хостел-радужный.рф",
      url: "https://xn----8sbmbisjucxhmts6h.xn--p1ai/"
    },
    {
      image: "/images/fifith.jpeg",
      description: "potolkot.ru",
      url: "http://ecoinzcom.artur4ik.beget.tech/"
    },
    {
      image: "/images/thirdt.jpeg",
      description: "podarok116.ru",
      url: "https://podarok116.ru"
    },
    {
      image: "/images/fortht.jpeg",
      description: "cs-guarantee.com",
      url: "https://cs-guarantee.com"
    },
    {
      image: "/images/second.jpeg",
      description: "gin-art.ru",
      url: "https://gin-art.ru"
    },
    {
      image: "/images/sixth.jpeg",
      description: "potolkot.ru",
      url: "https://potolkot.ru"
    },
    /* more portifolio items here */
  ],
  social: {
    twitter: "https://twitter.com/mitdeveloper",
    vk: "https://vk.com/mitdeveloper",
    github: "https://github.com/MIT-lab",
    email: "all@rkhakimov.ru",
    facebook: "https://www.facebook.com/100013556712300"
  },
  siteUrl: 'https://santosfrancisco.github.io/gatsbystarter-cv',
  pathPrefix: '/gatsby-starter-cv', // Note: it must *not* have a trailing slash.
  siteCover: '/images/cover.jpeg',
  googleAnalyticsId: 'UA-000000000-1',
  background_color: '#ffffff',
  theme_color: '#25303B',
  fontColor: "#000000cc",
  enableDarkmode: true, // If true, enables dark mode switch
  display: 'minimal-ui',
  icon: 'src/assets/gatsby-icon.png',
  headerLinks: [
    {
      label: 'Главная',
      url: '/',
    },
    {
      label: 'Портфолио',
      url: '/portifolio',
    }
  ]
}